#pragma once
#pragma warning(push)
#pragma warning(disable: 4251)

#ifdef CHRONOAPE_EXPORTS
    #define CHRONOAPE_API __declspec(dllexport)
#else
    #define CHRONOAPE_API __declspec(dllimport)
#endif

#include <string>
#include <chrono>


class CHRONOAPE_API ChronoApe {
public:
	ChronoApe();
    ChronoApe( const char * funcSig );
    ~ChronoApe();
private:
    std::string m_funcName;
    std::chrono::system_clock::time_point m_timeStart;
    std::chrono::system_clock::time_point m_timeEnd;
};

#pragma warning(pop)