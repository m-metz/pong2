#include "stdafx.h"
#include "GameBall.h"
#include "Game.h"
#include "GameObjectManager.h"
#include "PlayerPaddle.h"
#include "AIPaddle.h"
#include "ServiceLocator.h"

using namespace std;

GameBall::GameBall() :
_velocity( 400.0f ),
_elapsedTimeSinceStart( 0.0f ),
_runYet( false )
{
    Load( "images/ball.png" );
    assert( IsLoaded() );

    GetSprite().SetCenter( 15, 15 );
    sf::Randomizer::SetSeed( std::clock() );

    Initialize();
}

GameBall::~GameBall()
{
}

void GameBall::Initialize()
{
    // move to middle of the screen for now and randomize angle
    GetSprite().SetPosition( Game::SCREEN_WIDTH / 2, Game::SCREEN_HEIGHT / 2 );
    _angle = (float) sf::Randomizer::Random( 0, 359 );
    _velocity = 400.0f;
    _elapsedTimeSinceStart = 0.0f;
    _runYet = false;
}

void GameBall::Update( float elapsedTime )
{
    PlayerPaddle* player1 = dynamic_cast<PlayerPaddle*>( Game::GetGameObjectManager().Get( "Paddle1" ) );
    AIPaddle* player2 = dynamic_cast<AIPaddle*>( Game::GetGameObjectManager().Get( "Paddle2" ) );
    sf::Vector2f ballPos = GetPosition();
    _elapsedTimeSinceStart += elapsedTime;

    if ( _runYet == false )
    {
        // Delay game from starting until 3 seconds have passed
        if ( _elapsedTimeSinceStart > 3.0f )
        {
            _runYet = true;
        }
    }
    else
    {

        float moveByX = LinearVelocityX( _angle ) *  _velocity  * elapsedTime;
        float moveByY = LinearVelocityY( _angle ) *  _velocity  * elapsedTime;

        {
            //collide with the left side of the screen
            static bool canChangeDir = true;
            if ( ballPos.x + moveByX <= 0 + GetWidth() / 2 || ballPos.x + GetWidth() / 2 + moveByX >= Game::SCREEN_WIDTH )
            {
                if ( canChangeDir )
                {
                    //Ricochet!
                    _angle = 180.0f - _angle;
                    ValidateAngle( _angle );
                    if ( ( _angle >= 0.0f && _angle < 25.0f )
                        || ( _angle > 180.0f && _angle < 205.0f ) )
                    {
                        _angle += 25.0f;
                        ValidateAngle( _angle );
                    }
                    else if ( ( _angle > 335.0f && _angle < 360.0f )
                        || ( _angle > 145.0f && _angle <= 180.0f ) )
                    {
                        _angle -= 25.0f;
                        ValidateAngle( _angle );
                    }
                    moveByX = -moveByX;
                }
                canChangeDir = false;
            }
            else
            {
                canChangeDir = true;
            }
        }

        if ( player1 != NULL )
        {
            sf::Rect<float> p1BB = player1->GetBoundingRect();

            if ( p1BB.Intersects( GetBoundingRect() ) )
            {
                // Make sure ball isn't inside paddle
                if ( ballPos.y < player1->GetBoundingRect().Top )
                {
                    SetPosition( ballPos.x, player1->GetBoundingRect().Top - GetWidth() / 2 - 1 );

                    // Now add "English" based on the players velocity.  
                    _angle += player1->GetTopEnglish();
                    _angle = -_angle;
                    ValidateAngle( _angle );
                    moveByY = -moveByY;
                }
                else if ( ballPos.y > player1->GetBoundingRect().Bottom )
                {
                    SetPosition( ballPos.x, player1->GetBoundingRect().Bottom + GetHeight() / 2 + 1 );

                    _angle += player1->GetBottomEnglish();
                    _angle = -_angle;
                    ValidateAngle( _angle );
                    moveByY = -moveByY;
                }
                else if ( ballPos.x < player1->GetBoundingRect().Left )
                {
                    SetPosition( player1->GetBoundingRect().Left - GetWidth() / 2 - 1, ballPos.y );

                    _angle = 180.0f - _angle;
                    ValidateAngle( _angle );
                    moveByX = -moveByX;
                }
                else if ( ballPos.x > player1->GetBoundingRect().Right )
                {
                    SetPosition( player1->GetBoundingRect().Right + GetWidth() / 2 + 1, ballPos.y );

                    _angle = 180.0f - _angle;
                    ValidateAngle( _angle );
                    moveByX = -moveByX;
                }
                else
                {
                    SetPosition( ballPos.x, player1->GetBoundingRect().Top - GetWidth() / 2 - 1 );

                    _angle = -_angle;
                    ValidateAngle( _angle );
                    moveByY = -moveByY;
                }

                ServiceLocator::GetAudio()->PlaySound( "audio/blip.wav" );
                _velocity += 50.0f;
            }
        }

        if ( player2 != NULL )
        {
            sf::Rect<float> p2BB = player2->GetBoundingRect();

            if ( p2BB.Intersects( GetBoundingRect() ) )
            {
                // Make sure ball isn't inside paddle
                if ( ballPos.y < player2->GetBoundingRect().Top )
                {
                    SetPosition( ballPos.x, player2->GetBoundingRect().Top - GetWidth() / 2 - 1 );

                    _angle += player2->GetTopEnglish();
                    _angle = -_angle;
                    ValidateAngle( _angle );
                    moveByY = -moveByY;
                }
                else if ( ballPos.y > player2->GetBoundingRect().Bottom )
                {
                    SetPosition( ballPos.x, player2->GetBoundingRect().Bottom + GetHeight() / 2 + 1 );

                    _angle += player2->GetBottomEnglish();
                    _angle = -_angle;
                    ValidateAngle( _angle );
                    moveByY = -moveByY;
                }
                else if ( ballPos.x < player2->GetBoundingRect().Left )
                {
                    SetPosition( player2->GetBoundingRect().Left - GetWidth() / 2 - 1, ballPos.y );

                    _angle = 180.0f - _angle;
                    ValidateAngle( _angle );
                    moveByX = -moveByX;
                }
                else if ( ballPos.x > player2->GetBoundingRect().Right )
                {
                    SetPosition( player2->GetBoundingRect().Right + GetWidth() / 2 + 1, ballPos.y );

                    _angle = 180.0f - _angle;
                    ValidateAngle( _angle );
                    moveByX = -moveByX;
                }
                else
                {
                    SetPosition( ballPos.x, player2->GetBoundingRect().Bottom + GetHeight() / 2 + 1 );

                    _angle = -_angle;
                    ValidateAngle( _angle );
                    moveByY = -moveByY;
                }

                ServiceLocator::GetAudio()->PlaySound( "audio/blip.wav" );
            }
        }

        {
            // Hits top of screen
            //static bool canChangeDir = true;
            if ( ballPos.y - GetHeight() / 2 <= 0 )
            {
                Initialize();
                //if ( canChangeDir )
                //{
                //    _angle = -_angle;
                //    ValidateAngle( _angle );
                //    moveByY = -moveByY;
                //}
                //canChangeDir = false;
            }
            //else
            //{
            //    canChangeDir = true;
            //}
        }

        if ( ballPos.y + GetHeight() / 2 >= Game::SCREEN_HEIGHT )
        {
            Initialize();
        }

        GetSprite().Move( moveByX, moveByY );
    }
}

void GameBall::SetAngle( float angle )
{
    ValidateAngle( angle );
    _angle = angle;
}

void GameBall::ValidateAngle( float& angle )
{
    if ( angle < 0.0f )
    {
        angle += 360.0f;
    }
    else if ( angle >= 360.0f )
    {
        angle -= 360.0f;
    }
}

float GameBall::LinearVelocityX( float angle )
{
    return (float) std::cos( angle * ( 3.1415926 / 180.0f ) );
}

float GameBall::LinearVelocityY( float angle )
{
    return (float) std::sin( angle * ( 3.1415926 / 180.0f ) );
}

#ifdef _DEBUG
    namespace
    {
        deque<float> init()
        {
            deque<float> d;
            for ( size_t i = 0; i < 10; ++i )
            {
                d.push_back( -1.0f );
            }
            return d;
        };
    };
    deque<float> GameBall::dAngleHistory = init();
#endif