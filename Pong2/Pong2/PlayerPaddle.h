#pragma once
#include "VisibleGameObject.h"


class PlayerPaddle :
    public VisibleGameObject
{
public:
    PlayerPaddle();
    virtual ~PlayerPaddle();
    virtual void Update( float elapsedTime );
    virtual void Draw( sf::RenderWindow& rw );

    float GetVelocity() const;
    float GetTopEnglish() const;
    float GetBottomEnglish() const;

private:
    float _velocity;  // -- left ++ right
    float _maxVelocity;
    static float _acceleration;
};