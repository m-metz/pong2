// Pang.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Game.h"
#include <float.h>
#pragma fenv_access (on)

int main(int argc, _TCHAR* argv[])
{
    Game::Start();
	return 0;
}

// Overflow, divide-by-zero, and invalid-operation are the FP
// exceptions most frequently associated with bugs.
void FPExceptionEnabler( unsigned int enableBits = _EM_OVERFLOW |
    _EM_ZERODIVIDE | _EM_INVALID | _EM_OVERFLOW )
{
    static unsigned int mOldValues;
    // Retrieve the current state of the exception flags. This
    // must be done before changing them. _MCW_EM is a bit
    // mask representing all available exception masks.
    _controlfp_s( &mOldValues, 0, 0 );

    // Make sure no non-exception flags have been specified,
    // to avoid accidental changing of rounding modes, etc.
    enableBits &= _MCW_EM;

    // Clear any pending FP exceptions. This must be done
    // prior to enabling FP exceptions since otherwise there
    // may be a �deferred crash� as soon the exceptions are
    // enabled.
    _clearfp();

    // Zero out the specified bits, leaving other bits alone.
    _controlfp_s( 0, ~enableBits, enableBits );
}