#pragma once


namespace sf
{
    class RenderWindow;
}

class SplashScreen
{
public:
    enum SplashResult
    {
        Nothing, Exit, Continue
    };

    SplashResult Show( sf::RenderWindow &renderWindow );
};