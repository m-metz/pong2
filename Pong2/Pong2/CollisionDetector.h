/**
 * @author Michael Metz
 * @author MikeJohnMetz@gmail.com
 */

/**
 * @author Peter Kelley
 * @author pgkelley4@gmail.com
 *
 * Based off of Peter's code:
 *  https://github.com/pgkelley4/line-segments-intersect/blob/master/js/line-segments-intersect.js
 * See if two line segments intersect. This uses the 
 * vector cross product approach described below:
 * http://stackoverflow.com/a/565282/786339
 * 
 * @param {sf::Vector2<T>} p point object with x and y coordinates
 *  representing the start of the 1st line.
 * @param {Object} p2 point object with x and y coordinates
 *  representing the end of the 1st line.
 * @param {Object} q point object with x and y coordinates
 *  representing the start of the 2nd line.
 * @param {Object} q2 point object with x and y coordinates
 *  representing the end of the 2nd line.
 */
#pragma once
#include <SFML/System.hpp>


template <typename T>
bool doLineSegmentsIntersect( sf::Vector2<T> p, sf::Vector2<T> p2,
    sf::Vector2<T> q, sf::Vector2<T> q2 )
{
    sf::Vector2<T> r = subtractPoints( p2, p );
    sf::Vector2<T> s = subtractPoints( q2, q );

    sf::Vector2<T> uNumerator = crossProduct( subtractPoints( q, p ), r );
    sf::Vector2<T> denominator = crossProduct( r, s );

    if ( uNumerator == 0 && denominator == 0 )
    {
        // colinear, so do they overlap?
        return ( ( q.x - p.x < 0 ) != ( q.x - p2.x < 0 ) != ( q2.x - p.x < 0 ) != ( q2.x - p2.x < 0 ) ) ||
            ( ( q.y - p.y < 0 ) != ( q.y - p2.y < 0 ) != ( q2.y - p.y < 0 ) != ( q2.y - p2.y < 0 ) );
    }

    if ( denominator == 0 )
    {
        // lines are paralell
        return false;
    }

    var u = uNumerator / denominator;
    var t = crossProduct( subtractPoints( q, p ), s ) / denominator;

    return ( t >= 0 ) && ( t <= 1 ) && ( u >= 0 ) && ( u <= 1 );
}

/**
 * Calculate the cross product of the two points.
 * 
 * @param {Object} point1 point object with x and y coordinates
 * @param {Object} point2 point object with x and y coordinates
 * 
 * @return the cross product result as a float
 */
template <typename T>
sf::Vector2<T> crossProduct( sf::Vector2<T> point1, sf::Vector2<T> point2 )
{
    return point1.x * point2.y - point1.y * point2.x;
}

/**
 * Subtract the second point from the first.
 * 
 * @param {Object} point1 point object with x and y coordinates
 * @param {Object} point2 point object with x and y coordinates
 * 
 * @return the subtraction result as a point object.
 */ 
template <typename T>
sf::Vector2<T> subtractPoints( sf::Vector2<T> point1, sf::Vector2<T> point2 )
{
    sf::Vector2<T> result;
    result.x = point1.x - point2.x;
    result.y = point1.y - point2.y;

    return result;
}

//========== End of code based from Peter ============