#pragma once
#include "VisibleGameObject.h"
#ifdef _DEBUG
    #include <deque>
#endif

class GameBall :
    public VisibleGameObject
{
public:
    GameBall();
    virtual ~GameBall();
    void Update( float elapsedTime );
    void SetAngle( float angle );
    void ValidateAngle( float& angle );

private:
    float _velocity;
    float _angle;
    float _elapsedTimeSinceStart;
    bool _runYet;

    void Initialize();
    void HandleObjectCollision( VisibleGameObject* vgo, float moveByX, float moveByY );
    float LinearVelocityX( float angle );
    float LinearVelocityY( float angle );

    #ifdef _DEBUG
        static std::deque<float> dAngleHistory;
    #endif
};