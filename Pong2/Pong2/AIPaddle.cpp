#include "StdAfx.h"
#include "AIPaddle.h"
#include "Game.h"
#include "GameBall.h"

AIPaddle::AIPaddle() :
_velocity( 0 ),
_maxVelocity( 900.0f )
{
    Load( "images/paddle.png" );
    assert( IsLoaded() );

    GetSprite().SetCenter( GetSprite().GetSize().x / 2, GetSprite().GetSize().y / 2 );

}

AIPaddle::~AIPaddle()
{
}

void AIPaddle::Draw( sf::RenderWindow & rw )
{
    VisibleGameObject::Draw( rw );
}

float AIPaddle::GetVelocity() const
{
    return _velocity;
}

void AIPaddle::Update( float elapsedTime )
{
    static bool canChangeDir = false;

    const GameBall* gameBall = static_cast<GameBall*>( Game::GetGameObjectManager().Get( "Ball" ) );
    sf::Vector2f ballPosition = gameBall->GetPosition();
    //float ballVelocity = gameBall->GetVelocity();

    if ( GetPosition().x + 20 < ballPosition.x )
    {
        _velocity += _acceleration * elapsedTime;
    }
    else if ( GetPosition().x - 20 > ballPosition.x )
    {
        _velocity -= _acceleration * elapsedTime;
    }
    else if ( GetPosition().x + 5 < ballPosition.x && _velocity )
    {
        _velocity -= 2 * _acceleration * elapsedTime;
    }
    else if ( GetPosition().x - 5 > ballPosition.x )
    {
        _velocity += 2 * _acceleration * elapsedTime;
    }
    else if ( _velocity < 0.1f || _velocity > -0.1f )
    {
        _velocity = 0.0f;
    }


    if ( _velocity > _maxVelocity )
    {
        _velocity = _maxVelocity;
    }

    if ( _velocity < -_maxVelocity )
    {
        _velocity = -_maxVelocity;
    }

    sf::Vector2f pos = this->GetPosition();

    if ( pos.x  < GetSprite().GetSize().x / 2
        || pos.x >( Game::SCREEN_WIDTH - GetSprite().GetSize().x / 2 ) )
    {
        if ( canChangeDir )
        {
            canChangeDir = false;
            _velocity = -_velocity; // Bounce by current velocity in opposite direction
        }
    }
    else
    {
        canChangeDir = true;
    }

    GetSprite().Move( _velocity * elapsedTime, 0 );
}

float AIPaddle::GetTopEnglish() const
{
    if ( _velocity < 0 )
    {
        // moving left
        return 20.0f;
    }
    else if ( _velocity > 0 )
    {
        return -20.0f;
    }
    else
    {
        return 0.0f;
    }
}

float AIPaddle::GetBottomEnglish() const
{
    if ( _velocity < 0 )
    {
        // moving left
        return -20.0f;
    }
    else if ( _velocity > 0 )
    {
        return 20.0f;
    }
    else
    {
        return 0.0f;
    }
}

float AIPaddle::_acceleration = 1800.0f;