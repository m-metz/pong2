#include "stdafx.h"
#include "PlayerPaddle.h"
#include "Game.h"


PlayerPaddle::PlayerPaddle() :
_velocity( 0 ),
_maxVelocity( 900.0f )
{
    Load( "images/paddle.png" );
    assert( IsLoaded() );

    GetSprite().SetCenter( GetSprite().GetSize().x / 2, GetSprite().GetSize().y / 2 );
}


PlayerPaddle::~PlayerPaddle()
{
}

void PlayerPaddle::Draw( sf::RenderWindow & rw )
{
    VisibleGameObject::Draw( rw );
}

float PlayerPaddle::GetVelocity() const
{
    return _velocity;
}

void PlayerPaddle::Update( float elapsedTime )
{
    static bool canChangeDir = true;

    if ( Game::GetInput().IsKeyDown( sf::Key::Left ) )
    {
        if ( _velocity > 0 )
        {
            _velocity -= 4 * _acceleration * elapsedTime;
        }
        else
        {
            _velocity -= _acceleration * elapsedTime;
        }
    }
    if ( Game::GetInput().IsKeyDown( sf::Key::Right ) )
    {
        if ( _velocity < 0 )
        {
            _velocity += 4 * _acceleration * elapsedTime;
        }
        else
        {
            _velocity += _acceleration * elapsedTime;
        }
    }

    if ( Game::GetInput().IsKeyDown( sf::Key::Down ) )
    {
        _velocity = 0.0f;
    }

    if ( _velocity > _maxVelocity )
        _velocity = _maxVelocity;

    if ( _velocity < -_maxVelocity )
        _velocity = -_maxVelocity;


    sf::Vector2f pos = this->GetPosition();

    if ( pos.x  < GetSprite().GetSize().x / 2
        || pos.x >( Game::SCREEN_WIDTH - GetSprite().GetSize().x / 2 ) )
    {
        if ( canChangeDir )
        {
            _velocity = -_velocity; // Bounce by current velocity in opposite direction
        }
        canChangeDir = false;
    }
    else
    {
        canChangeDir = true;
    }

    GetSprite().Move( _velocity * elapsedTime, 0 );
}

float PlayerPaddle::GetTopEnglish() const
{
    if ( _velocity < 0 )
    {
        // moving left
        return -20.0f;
    }
    else if ( _velocity > 0 )
    {
        return 20.0f;
    }
    else
    {
        return 0.0f;
    }
}

float PlayerPaddle::GetBottomEnglish() const
{
    if ( _velocity < 0 )
    {
        // moving left
        return 20.0f;
    }
    else if ( _velocity > 0 )
    {
        return -20.0f;
    }
    else
    {
        return 0.0f;
    }
}

float PlayerPaddle::_acceleration = 1800.0f;