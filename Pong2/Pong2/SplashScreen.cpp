#include "stdafx.h"
#include "SplashScreen.h"


SplashScreen::SplashResult SplashScreen::Show( sf::RenderWindow &renderWindow )
{
    sf::Image image;
    if ( image.LoadFromFile( "Images/SplashScreen.png" ) != true )
    {
        return Nothing;
    }

    sf::Sprite sprite( image );

    renderWindow.Draw( sprite );
    renderWindow.Display();

    sf::Event event;
    while ( true )
    {
        renderWindow.GetEvent( event );
        if ( event.Type == sf::Event::Closed )
        {
            return Exit;
        }
        else if ( event.Type == sf::Event::KeyPressed
            || event.Type == sf::Event::MouseButtonPressed)
        {
            return Continue;
        }
    }
}